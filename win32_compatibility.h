/*
* IceBreaker
* Copyright (c) 2000-2001 Matthew Miller <mattdm@mattdm.org> and
*   Enrico Tassi <f.tassi@mo.nettuno.it>
* 
* <http://www.mattdm.org/icebreaker>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation; either version 2 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along
* with this program; if not, write to the Free Software Foundation, Inc., 59
* Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*/


/************************************************************************/
/* 
*  This file contains function prototypes, macros, etc. to aid in win32
*  cross-compiling using mingw32 (based on gcc version 2.95.2 19991024
*  (release))
*
*  This is Enrico Tassi's <f.tassi@mo.nettuno.it> domain. :)
*/


#ifndef WIN32_COMPATIBILITY_H
#define WIN32_COMPATIBILITY_H

/* mingw32 has _snprintf, not snprintf ??? */
#define snprintf _snprintf

/* mingw32 has no index(..) in string.h ??? */
extern char *index(const char *s, int c);

/* mingw32 has 'short' random functions */
#define srandom(A) srand(A)
#define random() rand()


/* No pwd.h in mingw32 */
#define uid_t int
#define gid_t int
	
struct passwd{
              char    *pw_name;       /* user name */
              char    *pw_passwd;     /* user password */
              uid_t   pw_uid;         /* user id */
              gid_t   pw_gid;         /* group id */
              char    *pw_gecos;      /* real name */
              char    *pw_dir;        /* home directory */
              char    *pw_shell;      /* shell program */
      };

/* a standard user -- see win32_compatibilty.c */
extern struct passwd pwdwin32_nobody;

/* No pwd.h -> no getpw... */	
#define getuid() 0
#define getpwuid(A) (&pwdwin32_nobody)

#endif
