/*
* IceBreaker
* Copyright (c) 2000-2001 Matthew Miller <mattdm@mattdm.org> and
*   Enrico Tassi <f.tassi@mo.nettuno.it>
* 
* <http://www.mattdm.org/icebreaker>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation; either version 2 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along
* with this program; if not, write to the Free Software Foundation, Inc., 59
* Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*/


/************************************************************************/
/* 
*  This file contains varibles and functions to aid in win32 cross-compiling
*  using mingw32 (based on gcc version 2.95.2 19991024 (release))
*
*  This is Enrico Tassi's <f.tassi@mo.nettuno.it> domain. :)
*/
 
#include "win32_compatibility.h"


/* This is the structure that getpwuid returns - I dont think w9X has uids */
struct passwd pwdwin32_nobody={"Nobody","*",1,1,"Win32 User",".","command.com"};

/* string.h of mingw32 has no index(char*) prototype, and so no implementation */
char *index(const char *s, int c)
{
	while( *s != '\0'&& *s != (char)c ) s++;
	return((char*)s);
}
